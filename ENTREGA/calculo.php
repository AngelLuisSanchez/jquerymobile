<?php
	/* Filtrar los datos */
	$peso = addslashes(htmlspecialchars($_POST["peso"]));
	$altura = addslashes(htmlspecialchars($_POST["altura"]));
	$altura = $altura / 100;

	if(empty($peso) || empty($altura)) echo "Rellene todos los campos por favor";
	else {	
		$resultado = $peso / ($altura * $altura);
		echo "<p>Tu IMC es <b>".$resultado."</b></p>";
	}
?>